<?php

    $lesErreurs = [];
    require 'model/TBaseDonnees.php';
    require 'model/TModelDB.php';
    require 'model/TEvenement.php';
    require 'model/TEvenementDB.php';
    require 'model/TPorte.php';
    require 'model/TPorteDB.php';
    require 'model/TTypeEvenement.php';
    require 'model/TUsager.php';
    require 'model/TUsagerDB.php';
