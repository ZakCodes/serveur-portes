<?php

    require_once 'config.php';

    // Commencer la session
    session_start();

    if (isset($_SESSION['usager'])) {
        $action = isset($_GET['action']) ? $_GET['action'] : 'portes';
    } else {
        $action = 'login';
    }

    try {
        switch ($action) {
            case 'login':
                include 'vue/login.php';
                break;
            case 'portes':
                include 'vue/portes.php';
                break;
            case 'logout':
                include 'vue/logout.php';
                break;
            default:
                $lesErreurs[] = 'Action invalide. Réessailler SVP fin ';
                include 'vue/erreur_saisie.php';
            break;
        }
    } catch (Exception $e) {
        $lesErreurs[] = 'Il y a eu une erreur de connexion à la base de données.';
        $lesErreurs[] = $e->getMessage();
        include 'vue/erreur_base_donnees.php';
    }
