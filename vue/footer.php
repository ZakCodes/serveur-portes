        </div>
        <footer class="d-flex justify-content-between p-2 bg-primary text-light font-weight-bold">
			<p>Projet PPS 2019</p>
            <p class="text-right">Fait sans aucun amour par <a href="https://gitlab.com/Sorrow">Izaak Beaudoin</a>, <a href="https://gitlab.com/1360445">Mathieu Berlinguette</a>, <a href="https://gitlab.com/Nhox-Ky">Sacha Collins</a>, <a href="https://gitlab.com/TheWave">Benoît Houle</a>, <a href="https://gitlab.com/marco999isback">Marc-Antoine Fleury-Tremblay</a> et <a href="https://gitlab.com/ZakCodes">Zakary Kamal Ismail</a></p>
        </footer>
    </body>
</html>
