<!DOCTYPE html>
<html lang="fr">
    <head>
	<meta charset="utf-8" />
        <title>Contrôleur de portes</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" />
        <link rel="stylesheet" type="text/css" href="vue/styles/main.css" />
    </head>

    <body class="d-flex flex-column" style="height: 100vh">
        <nav class="navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Serveur de portes</a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php?action=portes">Portes</a>
                    </li>
                    <?php if (isset($_SESSION['usager']) && $_SESSION['usager']->superUsager) { ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="/phpmyadmin/">Administration</a>
                    </li>
                    <?php } ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="manuel/">Manuel</a>
                    </li>
                </ul>

                <?php if (isset($_SESSION['usager'])) { ?>
                <a class="btn btn-outline-light my-2 my-sm-0" href="index.php?action=logout">Déconnexion</a>
                <?php } ?>
            </div>
        </nav>
        <div class="flex-grow-1">
