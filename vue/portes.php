<?php
include 'header.php';

if (isset($_GET['idporte'], $_GET['changement'])) {
    $porte = \PPS\TPorteDB::obtenirId(intval($_GET['idporte']));

    // Vérifie le changement demandé
    $nouvelEtat = 'verr' == $_GET['changement'];

    // Change l'état de la porte physique à déverrouillée.
    // @phan-suppress-next-line PhanNonClassMethodCall
    $resultat = $porte->changerEtatVerrou($nouvelEtat);

    // Si une erreur est survenue
    if (is_string($resultat)) {
        // Afficher le message d'erreur.
        echo "<div class=\"alert alert-danger mb-0\">{$resultat}</div>";
    } else {
        // Changer l'état de la porte dans la base de données à verrouillée.
        // @phan-suppress-next-line PhanTypeExpectedObjectPropAccessButGotNull
        $porte->verrouillee = $nouvelEtat;
        // @phan-suppress-next-line PhanTypeExpectedObjectPropAccessButGotNull
        \PPS\TPorteDB::modifier($porte);
    }
}
?>

<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">Nom</th>
            <th scope="col">Vérouillée</th>
            <th scope="col">Fermée</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach (\PPS\TPorteDB::liste() as $porte) { ?>
        <tr>
            <th scope="row"><?php echo $porte->nom; ?></th>
            <td>
                <a href="?action=portes&idporte=<?php echo $porte->id; ?>&changement=verr" class="btn <?php echo $porte->verrouillee ? 'btn-primary' : 'btn-secondary'; ?>">
                    <i class="fa fa-lock"></i>
                </a>
                <a href="?action=portes&idporte=<?php echo $porte->id; ?>&changement=deverr" class="btn <?php echo $porte->verrouillee ? 'btn-secondary' : 'btn-primary'; ?>">
                    <i class="fa fa-unlock"></i>
                </a>
            </td>
            <td>
                <i class="fa <?php echo $porte->fermee ? 'fa-door-closed' : 'fa-door-open'; ?>" style="font-size: 1.5rem;"></i>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>

<?php
    include 'footer.php';
?>
