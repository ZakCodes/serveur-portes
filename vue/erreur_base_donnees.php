

<?php
    include 'header.php';
?>

<section>
    <h1>Erreur à la base de données</h1>
    <ul>
        <?php foreach ($lesErreurs as $erreur) { // @phan-suppress-current-line PhanUndeclaredGlobalVariable?>
        <li><?php echo $erreur; ?></li>
        <?php } ?>
    </ul>
    <p>&nbsp;</p>
</section>

<?php include 'footer.php'; ?>
