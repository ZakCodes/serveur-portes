<?php
    include 'header.php';

    // Si l'usager est connecté
    if (isset($_SESSION['connecte']) && true === $_SESSION['connecte']) {
        // Rafraichir la page
        header('Refresh:0');
    }

    // Définir et initialiser les variables nom et mot de passe comme étant vide
    $id = $motDePasse = '';
    $id_err = $motDePasse_err = '';

    // Vérifier les données du POST lorsque envoyées
    if ('POST' == $_SERVER['REQUEST_METHOD']) {
        // Si le nom est vide
        if (empty(trim($_POST['id']))) {
            // Initialiser la variable de l'erreur nom
            $id_err = "Veuillez entrer un numéro d'utilisateur";
        } else {
            // Initialiser le nom entrer par l'utilisateur
            $id = trim($_POST['id']);
        }

        // Si le mot de passe est vide
        if (empty(trim($_POST['motDePasse']))) {
            // Initialiser la variable de l'erreur nom
            $motDePasse_err = 'Veuillez entrer un mot de passe';
        } else {
            // Initialiser le mot de passe entrer par l'utilisateur
            $motDePasse = trim($_POST['motDePasse']);
        }

        // Si le nom et le mot de passe n'ont pas eu d'erreur
        if (empty($id_err) && empty($motDePasse_err)) {
            // Demander l'usager à la base de données
            $usager = \PPS\TUsagerDB::obtenirId((int) $id);

            // Si l'usager existe
            // @phan-suppress-next-line PhanTypeExpectedObjectPropAccessButGotNull
            if (!is_null($usager->id)) {
                // @phan-suppress-next-line PhanTypeExpectedObjectPropAccessButGotNull
                if ($motDePasse === $usager->motDePasse) {
                    // Password is correct, so start a new session

                    // Store data in session variables
                    $_SESSION['connecte'] = true;
                    $_SESSION['usager'] = $usager;

                    // Rafraichir la page
                    header('Refresh:0');
                } else {
                    // Display an error message if password is not valid
                    $motDePasse_err = 'Le mot de passe est invalide';
                }
            } else {
                // Display an error message if username doesn't exist
                $id_err = "L'identifiant est invalide";
            }
        }
    }
?>
<div class="d-table h-100 container">
	<div class="d-table-cell align-middle">
	    <h2>Connexion</h2>
	    <p>Entrez les information de votre compte</p>
	    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
	        <div class="form-group <?php echo (!empty($id_err)) ? 'has-error' : ''; ?>">
	            <label><abbr title="Ceci est votre numéro d'étudiant ou d'employé">Numéro d'utilisateur</abbr></label>
	            <input type="text" name="id" class="form-control" value="<?php echo $id; ?>">
				<?php if (strlen($id_err) > 0) { ?>
					<div class="alert alert-danger mt-1"><?php echo $id_err; ?></div>
				<?php } ?>
	        </div>
	        <div class="form-group">
	            <label>Mot de passe</label>
	            <input type="password" name="motDePasse" class="form-control">
				<?php if (strlen($motDePasse_err) > 0) { ?>
					<div class="alert alert-danger mt-1"><?php echo $motDePasse_err; ?></div>
				<?php } ?>
	        </div>
	        <div class="form-group">
	            <input type="submit" class="btn btn-primary" value="Connexion">
	        </div>
	    </form>
	</div>
</div>
<?php
    include 'footer.php';
?>
