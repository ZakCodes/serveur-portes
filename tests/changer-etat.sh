#!/bin/sh

nom=$(printf 'sesame' | base64 -)
mdp=$(printf 'mot de passe' | base64 -)

url=http://localhost/serveur-portes/changer-etat.php

# Démarrage du contrôleur
curl $url -H "Authorization: Basic $nom:$mdp" \
    --data "001" -X POST \
    -w '\n%{http_code}\n'

# Ouverture de la porte
curl $url -H "Authorization: Basic $nom:$mdp" \
    --data "111" -X POST \
    -w '\n%{http_code}\n'

# Fermeture de la porte
curl $url -H "Authorization: Basic $nom:$mdp" \
    --data "111" -X POST \
    -w '\n%{http_code}\n'
