<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Test de l'interface pour contrôler les portes</title>
    </head>
    <body>
        <h1>Test de l'interface pour contrôler les portes</h1>
<?php
require_once '../model/TPorte.php';

$porte = new \PPS\TPorte(
    0,
    'nom',
    'mot de passe',
    'http://localhost:5002',
    true,
    true
);

// Verrouiller.
var_dump($porte->changerEtatVerrou(true));
echo '<br/><br/>';
var_dump($porte->etatPorte());
echo '<br/><br/>';

// Déverrouiller.
var_dump($porte->changerEtatVerrou(false));
echo '<br/><br/>';
var_dump($porte->etatPorte());
?>
    </body>
</html>
