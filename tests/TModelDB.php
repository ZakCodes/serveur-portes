<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Test de TModelDB</title>
    </head>
    <body>
        <h1>Test de TModelDB</h1>
<?php
require_once '../model/TBaseDonnees.php';
require_once '../model/TModelDB.php';
require_once '../model/TPorte.php';
require_once '../model/TPorteDB.php';

use PPS\TPorte;
use PPS\TPorteDB;

$porte = new TPorte(
    0,
    'nom',
    'mot de passe',
    'http://localhost:5002',
    true,
    true
);

if (!TPorteDB::inserer($porte)) {
    echo "Erreur d'insertion de la porte dans la base de données<br>";
}

$resultat = TPorteDB::obtenirId($porte->id);
if ($resultat != $porte) {
    var_dump($resultat);
    echo '<br>';
    var_dump($porte);
    echo '<br>';
    echo "Erreur d'obtention de la porte dans la base de données<br>";
}

if (!TPorteDB::modifier($porte)) {
    echo 'Erreur de modification de la porte dans la base de données<br>';
}

$liste = TPorteDB::liste();
$resultat = end($liste);
if ($resultat != $porte) {
    var_dump($resultat);
    echo '<br>';
    var_dump($porte);
    echo '<br>';
    echo 'Erreur de modification de la porte dans la base de données<br>';
}

if (!TPorteDB::supprimerId($porte->id)) {
    echo 'Erreur de suppression de la porte dans la base de données<br>';
}

?>
    </body>
</html>
