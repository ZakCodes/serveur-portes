# Tests

## Liste des tests
* [controleurs-portes.php](./controleurs-porte.php) test les communications entre le serveur et les contrôleurs de porte à l'aide des méthodes `TPorte::changerEtatVerrou` et `TPorte::etatPorte`

# Serveur de contrôleur de porte
Le dossier [`controleur-porte`](./controleur-porte/README.md) contient pour sa part une implémentation de serveur de contrôleur de porte.
Allez lire [son README](./controleur-porte/README.md) pour plus de détails.
