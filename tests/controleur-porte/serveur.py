#!/bin/env python

# Ce serveur est présentement écrit en Python, mais idéalement on devrait en faire 2 pages PHP
# réécrit en PHP pour qu'il soit démarré automatiquement lorsqu'on démarre le serveur.

from flask import Flask, request
from random import random
import base64
import sys

# Token pour accéder au serveur.
bearerToken = "mot de passe"

# Si la porte est verrouillée.
porteVerrouillee = 1

# Retourne un chaîne de caractères représentant l'état du système.
# Le 1er caractère signifie que la porte est vérouillée (1) ou déverrouillée (0).
# Le 2e caractère signifie que la porte est fermée (1) ou ouverte (0).
# La valeur du 1er dépend de la variable globale porteVerrouillee.
# La valeur du 2e est générée aléatoirement à chaque appel de la fonction.
def etat():
    global porteVerrouillee
    return f"{porteVerrouillee}{int(random() > 0.5)}"

# S'assure que la requête comporte le bon bearer token
# Retourne True si c'est le cas, et False sinon
def authentifier(requete):
    expected_scheme = "bearer "
    expected_scheme_len = len(expected_scheme)
    header = requete.headers.get("Authorization")

    if header == None:
        return False

    scheme = header[:expected_scheme_len];
    if scheme.lower() == expected_scheme:
        try:
            token = header[expected_scheme_len:].encode()
            token = base64.decodebytes(token).decode()
            return token == bearerToken
        except:
            return False
    return False

app = Flask(__name__)

# Authentifie les requêtes, change l'état du système pour celui demandé
# et retourne l'état du système
@app.route('/verrou', methods = ['POST'])
def post_verrou():
    global porteVerrouillee
    if authentifier(request):
        statut = 400
        if request.data.startswith(b'0'):
            statut = 200 if 0 != porteVerrouillee else 202
            porteVerrouillee = 0
        elif request.data.startswith(b'1'):
            statut = 200 if 1 != porteVerrouillee else 202
            porteVerrouillee = 1

        return etat(), statut
    else:
        return "Erreur d'Authentification", 401

# Authentifie les requêtes et retourne l'état du système
@app.route('/etat', methods=['GET'])
def get_etat():
    if authentifier(request):
        return etat(), 200
    else:
        return "Erreur d'Authentification", 401

# Démarrer le serveur.
app.run(port='5002')
