<?php

// Authentifie les requêtes, change l'état du système pour celui demandé
// et retourne l'état du système.
require_once '../commun.php';

// Déclarer le type du contenu comme du texte brut.
header('Content-Type: text/plain');

// Initialiser le statut de la réponse au statut d'authorization invalide.
$statut = 401;

// Authentifier le client.
// Si le client a pu être authentifié
if (authentifier()) {
    // Assigner le statut de réussite à la réponse.
    $statut = 200;

    // Écrire l'état du système dans la réponse.
    echo etat();
} else {
    // Écrire un message d'erreur dans la réponse.
    echo "Erreur d'Authentification";
}

// Ajouter le statut à la réponse.
http_response_code($statut);
