<?php

// Index de l'entête d'authorization dans $GLOBALS.
define('ENTETE_AUTHORIZATION', 'Authorization');

// Schéma d'authentification attendu dans les requêtes.
define('SCHEME_AUTHENTIFICATION', 'bearer');

// Token pour accéder au serveur.
define('BEARER_TOKEN', 'mot de passe');

// Si la porte est verrouillée.
$GLOBALS['porteVerrouillee'] = 1;

/*!
    @brief  Retourne une chaîne de caractères représentant l'état du système.
    @return une chaîne de caractères représentant l'état du système.
        Le 1er caractère signifie que la porte est vérouillée (1) ou déverrouillée (0).
        Le 2e caractère signifie que la porte est fermée (1) ou ouverte (0).
        La valeur du 1er dépend de la variable globale porteVerrouillee.
        La valeur du 2e est générée aléatoirement à chaque appel de la fonction.
    @retval "00"
    @retval "01"
    @retval "10"
    @retval "11"
*/
function etat() {
    return $GLOBALS['porteVerrouillee'].rand(0, 1);
}

/*!
    @brief  S'assure que la requête comporte le bon bearer token.
    @return si la requête est correctement authentifiée
    @retval true la requête est authentifiée
    @retval false la requête n'est pas authentifiée
*/
function authentifier(): bool {
    // Initialiser le résultat à la valeur signifiant que le client
    // n'est pas authentifier.
    $estAuthentifie = false;

    // Calculer le préfixe de l'entête d'authorization.
    $prefixEntete = SCHEME_AUTHENTIFICATION.' ';
    $taillePrefix = strlen($prefixEntete);

    // Obtenir la liste d'entêtes.
    $entetes = apache_request_headers();

    // Si l'entête comporte une entête d'authorization.
    if (isset($entetes[ENTETE_AUTHORIZATION])) {
        $entete = $entetes[ENTETE_AUTHORIZATION];

        // Si le schema d'authorization est celui attendu
        $scheme = substr($entete, 0, $taillePrefix);
        if (strtolower($scheme) == $prefixEntete) {
            try {
                // Décoder le token de la requête en base 64.
                $token = base64_decode(substr($entete, $taillePrefix));

                // Comparer le token avec celui attendu.
                // Assigner true au résultat s'ils sont égaux.
                $estAuthentifie = BEARER_TOKEN == $token;
            } catch (Exception $e) {
            }
        }
    }

    // Retourné si le client est authentifié à la routine appelante.
    return $estAuthentifie;
}
