# Serveur de contrôleur de porte
Ce dossier contient deux implémentations de serveurs qui émule un serveur de contrôleur de porte. L'état retourné par ses deux implémentation est celui configuré pour le verrou et une état aléatoire à chaque requête pour l'ouverture.

## Serveur Python
Pour utiliser le serveur de test en Python, il faudra tout d'abord que vous [installiez Python](https://www.python.org/downloads/). **Lors de l'installation, assurez vous d'ajouter Python à votre `PATH`**.

Une fois fait, vous aurez besoin d'installer la librarie [flask] avec pip comme ceci:
```sh
pip install flask
```
Vous aurez besoin des droits d'administrateurs pour cette commande.

Une fois cela fait, vous n'avez qu'à démarrer le serveur avec la commande `python serveur.py` où `serveur.py` est le chemin vers le fichier Python.

Vous pouvez désormais accéder au serveur à l'adresse <http://localhost:5002/>.

## Serveur PHP
Les fichiers PHP dans ce dossier permettent d'émuler un serveur de contrôleur de porte.
Vous pouvez y accéder à partir de l'adresse <http://localhost/serveur-portes/tests/controleur-porte>.  

**Attention, il ne peut pas être utilisé à partir de ce projet.**  
En effet, il ne fonctionne pas à partir de ce projet pour la raison qui est expliquée [ici](https://stackoverflow.com/questions/5412069/can-i-do-a-curl-request-to-the-same-server). Une solution est aussi donnée, mais elle ne semble pas fonctionner.

**Utilisez donc le serveur en Python.**
