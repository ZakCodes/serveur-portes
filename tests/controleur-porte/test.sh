#!/bin/sh

# Ce script test le serveur de test de contrôleur de porte.
# Il peut cependant être utilisé pour tester n'importe quel
# autre serveur qui est compatible avec ce dernier.
# Vous n'avez qu'à modifier le mot de passe et l'URL pour le faire fonctionner.

# Mot de passe du serveur encodé en base 64.
token=$(printf 'mot de passe' | base64  -)

# Chemin de base du serveur.
# Assurez vous de ne pas finir le URL par un '/'.
url="http://localhost:5002"

# Verrouiller la porte.
curl --request POST \
    --header "Content-Type: application/json" \
    --oauth2-bearer $token \
    --data '1' \
    -w "\n%{http_code}\n" \
    -L $url/verrou
echo

# Déverrouiller la porte.
curl --request POST \
    --header "Content-Type: text/plain" \
    --oauth2-bearer $token \
    --data '1' \
    -w "\n%{http_code}\n" \
    -L $url/verrou
echo

# Affciher l'état de la porte.
curl --header "Content-Type: text/plain" \
    --oauth2-bearer $token \
    -w "\n%{http_code}\n" \
    -L $url/etat
echo
