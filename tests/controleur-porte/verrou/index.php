<?php

// Authentifie les requêtes, change l'état du système pour celui demandé
// et retourne l'état du système.
require_once '../commun.php';

// Déclarer le type du contenu comme du texte brut.
header('Content-Type: text/plain');

// Initialiser le statut de la réponse au statut d'authorization invalide.
$statut = 401;

// Authentifier le client.
// Si le client a pu être authentifié
if (authentifier()) {
    // Initialiser un statut au statut de requête invalide.
    $statut = 400;

    // Obtenir l'état demandé.
    $contenu = file_get_contents('php://input');
    var_dump(apache_request_headers());
    echo '<br/><br/>';
    var_dump($contenu);
    echo '<br/><br/>';
    $etatDemande = substr($contenu, 0, 1);
    // $etatDemande = substr($HTTP_RAW_POST_DATA, 0, 1);
    // $etatDemande = substr($GLOBALS["HTTP_RAW_POST_DATA"], 0, 1);

    // $etatDemande = substr(fgets(STDIN), 0, 1);
    if ('0' == $etatDemande) {
        $etatDemande = 0;
    } elseif ('1' == $etatDemande) {
        $etatDemande = 1;
    }

    // Si l'état demandé était valide
    if (is_int($etatDemande)) {
        // Obtenir l'ancien état.
        $ancienEtat = $GLOBALS['porteVerrouillee'];

        // Changer l'état pour celui demandé.
        $GLOBALS['porteVerrouillee'] = $etatDemande;

        // Assigner le statut 200 à la réponse si l'état demandé est différent de l'ancien.
        // Sinon lui assigner 202.
        $statut = $ancienEtat == $etatDemande ? 200 : 202;
    } // Fin du si.

    // Écrire l'état du système dans la réponse.
    echo etat();
} else {
    // Écrire un message d'erreur dans la réponse.
    echo "Erreur d'Authentification";
}

// Ajouter le statut à la réponse.
http_response_code($statut);
