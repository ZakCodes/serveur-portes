<?php

namespace PPS {
    //! USER_AGENT utilisé pour envoyer des requêtes aux contrôleurs de portes.
    const USER_AGENT = 'Serveur de contrôle de porte';

    //! Traduction française des [codes d'erreurs de cURL](https://curl.haxx.se/libcurl/c/libcurl-errors.html)
    //! retournés par curl_errno
    const MSG_ERREURS_CURL = [
        -1 => "Le contrôleur n'a pas retourné son état",
        0 => 'Aucune erreur',
        2 => "Erreur d'initialisation de cURL",
        3 => 'URL du contrôleur de porte invalide',
        4 => "Le proxy du contrôleur de porte n'a pas pu être résolu",
        5 => "Le proxy n'a pas pu être résolu",
        6 => "L'hôte n'a pas pu être résolu",
        7 => "Connexion à l'hôte ou au proxy échouée",
        16 => 'Erreur du protocole HTTP/2',
        18 => 'La taille du fichier transféré par le contrôleur de porte est inférieur à la taille attendue',
        21 => 'Ereur de type 400',
        23 => "Erreur lors de l'envoi du corps de la requête",
        26 => 'Erreur lors de la réception du corps de la réponse',
        27 => "Erreur d'allocation de mémoire",
        28 => 'Un temps mort est survenu lors de la communication',
        35 => "Erreur d'initialisation du canal SSL/TLS",
        45 => "Une interface réseau n'a pas pu être utilisée",
        47 => 'Nombre de redirections maximal dépassé',
        54 => "La librarie de cryptographie par défaut n'a pas pu être utilisée",
        55 => "Erreur de réseau lors de l'envoi de la requête",
        56 => 'Erreur de réseau lors de la réception de la réponse',
        58 => 'Erreur de certificat SSL',
        60 => "Le certificat du serveur n'a pas pu être vérifié",
        61 => 'Encodage du contenu reçu inconnu',
        63 => 'Taille maximum du fichier reçu dépassée',
        66 => 'Initialisation de la librarie SSL ratée',
        75 => "Conversion d'un caractère ratée",
        76 => 'Conversion non configurée',
        77 => 'Accès refusé au fichier du certificat',
        81 => "Réesayez plus tard d'envoyer une requête, car le serveur n'est pas prêt",
        91 => 'Certificat SSL invalide',
        92 => 'Erreur de flux du protocl HTTP/2',
        94 => "Erreur d'authentification",
    ];
    //! Message pour tous les autres codes d'erreur pouvant être retournés par cURL.
    const MSG_ERREUR_CURL_DEFAUT = 'Erreur cURL inconnue';

    //! Traduction française des [codes d'erreurs de cURL](https://curl.haxx.se/libcurl/c/libcurl-errors.html)
    //! retournés par curl_errno
    const MSG_STATUTS_HTTP = [
        200 => 'Aucune erreur',
        202 => 'État du verrou inchangé',
        400 => 'Erreur de requête mal formée',
        401 => "Erreur d'authentification. Le mot de passe utilisé est probablement invalide.",
        500 => 'Erreur du contrôleur de porte',
    ];
    //! Message pour tous les autres codes d'erreur pouvant être retournés par cURL.
    const MSG_STATUT_HTTP_INCONNU = 'Statut HTTP inconnu retourné par le contrôleur de porte';

    //! Informations pour communiquer avec un contrôleur de porte et sur son état.
    class TPorte {
        //! Identifiant unique de la porte. Utilisé par la base de données.
        /** @var int */
        public $id;

        //! Nom de la porte.
        /** @var string */
        public $nom;

        //! Mot de passe pour accèder à la porte.
        //! Attention, ne pas encoder cette valeur en base 64.
        /** @var string */
        public $motDePasse;

        //! URL de base de la porte.
        /** @var string */
        public $url;

        //! Si la porte est fermée.
        /** @var bool */
        public $fermee;

        //! Si la porte est verrouillée.
        /** @var bool */
        public $verrouillee;

        /*!
            @brief  Initialise une porte avec les propriétés fournies
        */
        public function __construct(
            //! Identifiant unique de la porte dans la base de données
            int $id,
            //! Nom de la porte
            string $nom,
            //! Mot de passe de la porte
            string $motDePasse,
            //! URL de base de la porte
            string $url,
            //! Si la porte est fermée
            bool $fermee,
            //! Si la porte est vérouillée
            bool $verrouillee
        ) {
            $this->id = $id;
            $this->nom = $nom;
            $this->motDePasse = $motDePasse;
            $this->url = $url;
            $this->fermee = $fermee;
            $this->verrouillee = $verrouillee;
        }

        /*!
            @brief  Changer l'état du contrôleur de porte pour celui donné

            @return string|array
            @retval Un message d'erreur
            @retval liste contenant les éléments suivants:
                    verrou (bool): Si la porte est vérouillée
                    ouverture (bool): Si la porte est ouverte
                    etatVerrouChange (bool): Si l'état du verrou à changé
        */
        public function changerEtatVerrou(
            //! Si la porte doit être vérouillée
            bool $verrouillee
        ) {
            // Envoyer une requête au contrôleur de porte pour modifier l'état du verrou.
            $reponse = $this->envoyerRequete('/verrou', true, (string) (int) $verrouillee);

            // Initialiser le résultat au message d'erreur de la réponse.
            $resultat = $reponse['message'];

            // S'il n'y a pas eu d'erreur.
            if (0 === $reponse['errno']) {
                // @phan-suppress-next-line PhanTypePossiblyInvalidDimOffset
                $statut = $reponse['statut'];

                if (200 == $statut || 202 == $statut) {
                    $resultat = [
                        // @phan-suppress-next-line PhanTypePossiblyInvalidDimOffset
                        'verrou' => $reponse['verrou'],
                        // @phan-suppress-next-line PhanTypePossiblyInvalidDimOffset
                        'ouverture' => $reponse['ouverture'],
                        'etatVerrouChange' => 200 == $statut,
                    ];
                } else {
                    $resultat = MSG_STATUT_HTTP_INCONNU.': '.$statut;
                }
            }

            // Retourner le résultat à la routine appelante.
            return $resultat;
        }

        /*!
            @brief  Changer l'état du contrôleur de porte pour celui donné

            @return string|array
            @retval Un message d'erreur
            @retval liste contenant les éléments suivants:
                    verrou (bool): Si la porte est vérouillée
                    ouverture (bool): Si la porte est ouverte
        */
        public function etatPorte() {
            // Envoyer une requête au contrôleur de porte pour obtenir son état.
            $reponse = $this->envoyerRequete('/etat');

            // Initialiser le résultat au message d'erreur de la réponse.
            $resultat = $reponse['message'];

            // S'il n'y a pas eu d'erreur.
            if (0 === $reponse['errno']) {
                // @phan-suppress-next-line PhanTypePossiblyInvalidDimOffset
                $statut = $reponse['statut'];

                if (200 == $statut) {
                    $resultat = [
                        // @phan-suppress-next-line PhanTypePossiblyInvalidDimOffset
                        'verrou' => $reponse['verrou'],
                        // @phan-suppress-next-line PhanTypePossiblyInvalidDimOffset
                        'ouverture' => $reponse['ouverture'],
                    ];
                } else {
                    $resultat = MSG_STATUT_HTTP_INCONNU.': '.$statut;
                }
            }

            // Retourner le résultat à la routine appelante.
            return $resultat;
        }

        /*!
            @brief  Utilise cURL pour envoyer une requête au contrôleur de porte

            @return array
            @retval Liste qui contient le résultat de la requête, si le résultat
                    est négatif. La liste contient tous ces éléments:
    				errno (int != 0): code d'erreur de cURL ainsi que le code -1 qui est
    				 	   utilisé pour signifier que le contrôleur de porte n'a pas
    					   retourner son état.
    				message (string): message traduit en français associé au code d'erreur.
            @retval Liste qui contient le résultat de la requête et l'état du
                    contrôleur de porte, lorsqu'aucune erreur ne survient.
    		 	 	La liste contient tous ces éléments:
    				errno (int = 0): code d'erreur de cURL ainsi que le code -1 qui est
    				 	   utilisé pour signifier que le contrôleur de porte n'a pas
    					   retourner son état.
    				message (string): message traduit en français associé au code d'erreur.
    				statut (string): Statut HTTP retourné par le serveur.
    				                    Cette valeur n'est présente que
    									si errno est égal à 0.
    				verrou (bool): Si la porte est vérouillée
    				ouverture (bool): Si la porte est ouverte
        */
        private function envoyerRequete(
            //! Chemin de la requête.
            string $chemin,
            //! Si la méthode HTTP de la requête est POST, par défaut, GET est utilisé.
            bool $methodePost = false,
            //! Corps de la requête, par défaut vide.
            string $corps = null
        ) {
            // Configurer les propriétés communes à toutes les requêtes.
            $options = [
                CURLOPT_HTTPHEADER => [
                    'Authorization: Bearer '.base64_encode($this->motDePasse),
                ],
                CURLOPT_USERAGENT => USER_AGENT,
                CURLOPT_FAILONERROR => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_TIMEOUT => 5,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_OPTIONS => CURLSSLOPT_NO_REVOKE,
            ];
            $options[CURLOPT_POST] = $methodePost;

            // Configurer les propriétés pour envoyer un corps avec la requête.
            $compte = 0;
            if (null != $corps) {
                $options[CURLOPT_HTTPHEADER][] = 'Content-type: text/plain';
                $options[CURLOPT_HTTPHEADER][] = 'Content-Length: '.strlen($corps);
                $options[CURLOPT_POSTFIELDS] = $corps;
            }

            // Configurer les propriétés pour recevoir le corps de la réponse.
            $reponse = '';
            $callback_reponse = function ($conn, $contenu) use (&$reponse) {
                $reponse = $contenu;
                return strlen($contenu);
            };
            $options[CURLOPT_WRITEFUNCTION] = $callback_reponse;

            // Envoyer la requête.
            $conn = curl_init(rtrim($this->url, '/').$chemin);
            curl_setopt_array($conn, $options);
            curl_exec($conn);

            // Lire la réponse et créer le résultat de la fonction.
            $resultat = [];
            $errno = curl_errno($conn);

            $message = null;

            // S'il n'y a aucune erreur
            if (0 === $errno) {
                // Obtenir la réponse.
                $resultat['statut'] = curl_getinfo($conn, CURLINFO_RESPONSE_CODE);

                // Si le contrôleur a retourné son état.
                if (strlen($reponse) >= 2) {
                    // Lire l'état dans le corps.
                    $resultat['verrou'] = '0' !== $reponse[0];
                    $resultat['ouverture'] = '0' !== $reponse[1];
                } else {
                    // Assigner l'erreur de corps inexistant au code d'erreur.
                    $errno = -1;
                }
            }
            // Si un code d'erreur HTTP a été reçu
            elseif (22 === $errno) {
                $statut = curl_getinfo($conn, CURLINFO_RESPONSE_CODE);

                if (isset(MSG_STATUTS_HTTP[$statut])) {
                    $message = MSG_STATUTS_HTTP[$statut];
                } else {
                    $message = MSG_STATUT_HTTP_INCONNU.': '.$statut;
                }
            } else {
                if (isset(MSG_ERREURS_CURL[$errno])) {
                    $message = MSG_ERREURS_CURL[$errno];
                } else {
                    $message = MSG_ERREUR_CURL_DEFAUT.': '.$errno;
                }
            } // Fin du si.

            $resultat['message'] = $message;
            $resultat['errno'] = $errno;

            // Fermer la session cURL.
            curl_close($conn);

            // Retourner le résultat à la routine appelante.
            return $resultat;
        }
    }
}
