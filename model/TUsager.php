<?php

namespace PPS {
    class TUsager {
        public $id;
        public $nom;
        public $prenom;
        public $email;
        public $motDePasse;
        public $superUsager;

        public function __construct($id, $nom, $prenom, $email, $motDePasse, $superUsager) {
            $this->id = $id;
            $this->nom = $nom;
            $this->prenom = $prenom;
            $this->email = $email;
            $this->motDePasse = $motDePasse;
            $this->superUsager = $superUsager;
        }
    }
}
