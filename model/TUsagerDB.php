<?php

namespace PPS {
    class TUsagerDB extends TModelDB {
        /*!
            @brief  Voir TModelDB::props
        */
        public static function props(): array {
            return [
                'id' => ['id', \PDO::PARAM_INT],
                'nom' => ['nom', \PDO::PARAM_STR],
                'prenom' => ['prenom', \PDO::PARAM_STR],
                'email' => ['email', \PDO::PARAM_STR],
                'motDePasse' => ['mot_De_Passe', \PDO::PARAM_STR],
                'superUsager' => ['superUsager', \PDO::PARAM_BOOL],
            ];
        }

        /*!
            @brief  Voir TModelDB::table
        */
        public static function table(): string {
            return 'usager';
        }

        /*!
            @brief  Voir TModelDB::defaut
        */
        public static function defaut() {
            return new TUsager(0, '', '', '', '', false);
        }
    }
}
