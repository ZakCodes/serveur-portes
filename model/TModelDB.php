<?php

namespace PPS {
    //! Interface utilisée par TModelDB
    interface IModelDB {
        /*!
            @brief  Retourne la liste associative des noms des propriétés de l'objet
                    en PHP et dans la base de données et de leur type. La clé
                    est le nom de la propriété en PHP et la valeur est une autre
                    liste dont le premier élément est le nom de la propriété dans
                    la base de données et son type qui est une valeur de PDO::PARAM_*.
                    Le premier élément doit être la propriété de l'ID unique des
                    instances de ce type.
            @return array La liste de propriétés de la classe
        */
        public static function props(): array;

        /*!
            @brief  Retourne le nom de la table contenant les objets de cette
                    classe dans la base de données
            @return string Le nom de la table
        */
        public static function table(): string;

        /*!
            @brief  Retourne un objet initialisé avec des valeurs par défaut de votre type.
            @return Self Un objet initialisé de votre type
        */
        public static function defaut();
    }

    //! Interface CRUD pour emmagasiner les objets d'une classe dans
    //! la base de données.
    abstract class TModelDB implements IModelDB {
        /*!
            @brief  Obtient la liste de tous les objets du type dans la base de données
            @return array Une liste d'objets
        */
        public static function liste(): array {
            // Obtenir le nom de la classe et ses propriétés.
            $classe = get_called_class();
            $props = $classe::props();
            $table = $classe::table();

            // Établir la connexion à la base de données.
            $conn = TBaseDonnees::getBaseDonnee();

            // Obtenir toutes les données de la table.
            $sql = "SELECT * FROM {$table}";

            // Placer les données des objets dans un tableau.
            $resultats = $conn->query($sql);

            // Obtenir tous les objets du résultat obtenu.
            $objets = [];
            foreach ($resultats as $rangee) {
                // Initialiser un nouvel objet avec des propriétés par défaut.
                $obj = $classe::defaut();

                // Copier chaque propriété dans l'objet.
                foreach ($props as $php => $db) {
                    $obj->{$php} = $rangee[$db[0]];
                }

                // Ajouter l'objet à la liste.
                $objets[] = $obj;
            }

            // Fermer la connexion.
            $conn = null;

            // Retourner la liste d'objets.
            return $objets;
        }

        /*!
            @brief  Retourne un objet avec l'id donné dans la base de données
            @return Self? Un objet trouvé ou null
            @retval Self L'objet trouvé
            @retval null Aucun objet n'a été trouvé avec l'id donné
        */
        public static function obtenirId($id) {
            // Obtenir le nom de la classe et ses propriétés.
            $classe = get_called_class();
            $props = $classe::props();
            $table = $classe::table();

            $idPHP = array_key_first($props);
            $idDB = $props[$idPHP];
            $idNomDB = $idDB[0];
            $idTypeDB = $idDB[1];

            // Établir la connexion à la base de données.
            $conn = TBaseDonnees::getBaseDonnee();

            // Obtenir l'objet avec l'ID donné.
            $sql = "SELECT * FROM {$table} WHERE {$idNomDB}=:id";
            $prep = $conn->prepare($sql);
            $prep->bindValue('id', $id, $idTypeDB);

            // Initialiser un résultat nul.
            $obj = null;

            // Exécuter la commande.
            // Si aucune erreur n'est survenue
            if (false != $prep->execute()) {
                // Obtenir le résultat de la commande.
                $rangee = $prep->fetch();

                // Initialiser un nouvel objet avec des propriétés par défaut.
                $obj = $classe::defaut();

                // Copier chaque propriété dans l'objet.
                foreach ($props as $php => $db) {
                    $obj->{$php} = $rangee[$db[0]];
                }
            }

            // Fermer la connexion.
            $conn = null;

            // Retourner l'objet à la fonction appelante.
            return $obj;
        }

        /*!
            @brief  Insère l'objet donné dans la base de données
            @return boolean Si l'opération a été réussite
            @retval true L'opération a été réussite
            @retval false L'opération a échoué
        */
        public static function inserer($obj) {
            // Obtenir le nom de la classe et ses propriétés.
            $classe = get_called_class();
            $props = $classe::props();
            $table = $classe::table();

            $idPHP = array_key_first($props);
            $idDB = $props[$idPHP];
            $idNomDB = $idDB[0];
            $idTypeDB = $idDB[1];

            // Établir la connexion à la base de données.
            $conn = TBaseDonnees::getBaseDonnee();

            // Insérer les données de l'objet dans la base de données.
            $fn = function ($el) {
                return $el[0];
            };
            $sql = "INSERT INTO {$table} (".join(', ', array_map($fn, array_slice($props, 1))).')
            VALUES ('.join(', ', array_fill(0, sizeof($props) - 1, '?')).')';
            $prep = $conn->prepare($sql);

            $i = 1;
            foreach (array_slice($props, 1) as $php => $db) {
                $prep->bindValue($i, $obj->{$php}, $db[1]);
                ++$i;
            }

            // Initialiser le résultat à la routine appelante.
            $reussite = true;

            // Exécuter la commande.
            // Si aucune erreur n'est survenue
            if (false != $prep->execute()) {
                $obj->id = (int) $conn->lastInsertId();
            } else {
                error_log("Erreur lors de l'insertion de l'objet de classe {$classe}: ".$conn->errorInfo()[2]);
                $reussite = false;
            }

            // Fermer la connexion.
            $conn = null;

            // Retourner le résultat à la routine appelante.
            return $reussite;
        }

        /*!
            @brief  Supprimer un objet avec l'id donné
            @return boolean Si l'opération a été réussite
            @retval true L'opération a été réussite
            @retval false L'opération a échoué
        */
        public static function supprimerId($id) {
            // Obtenir le nom de la classe et ses propriétés.
            $classe = get_called_class();
            $props = $classe::props();
            $table = $classe::table();

            $idPHP = array_key_first($props);
            $idDB = $props[$idPHP];
            $idNomDB = $idDB[0];
            $idTypeDB = $idDB[1];

            // Établir la connexion à la base de données.
            $conn = TBaseDonnees::getBaseDonnee();

            // Préparer la commande.
            $sql = "DELETE FROM {$table} WHERE {$idNomDB}=:id";
            $prep = $conn->prepare($sql);
            $prep->bindValue('id', $id, $idTypeDB);

            // Initialiser le résultat à la routine appelante.
            $reussite = true;

            // Exécuter la commande.
            // Si aucune erreur n'est survenue
            if (false == $prep->execute()) {
                error_log("Erreur lors de l'effacement d'un objet de la classe {$classe}: ".$conn->errorInfo()[2]);
                $reussite = false;
            }

            // Fermer la connexion.
            $conn = null;

            // Retourner le résultat à la routine appelante.
            return $reussite;
        }

        /*!
            @brief  Modifie l'objet donné dans la base de données à l'aide de son ID
            @return boolean Si l'opération a été réussite
            @retval true L'opération a été réussite
            @retval false L'opération a échoué
        */
        public static function modifier($obj) {
            // Obtenir le nom de la classe et ses propriétés.
            $classe = get_called_class();
            $props = $classe::props();
            $table = $classe::table();

            $idPHP = array_key_first($props);
            $idDB = $props[$idPHP];
            $idNomDB = $idDB[0];
            $idTypeDB = $idDB[1];

            // Établir la connexion à la base de données.
            $conn = TBaseDonnees::getBaseDonnee();

            // Préparer la commande.
            $fn = function ($el) {
                return $el[0].'=?';
            };
            $sql = "UPDATE {$table}
                    SET ".join(', ', array_map($fn, array_slice($props, 1))).
                    " WHERE {$idNomDB}=?";
            $prep = $conn->prepare($sql);
            $i = 1;
            foreach (array_slice($props, 1) as $php => $db) {
                $prep->bindValue($i, $obj->{$php}, $db[1]);
                ++$i;
            }
            $prep->bindValue($i, $obj->{$idNomDB}, $idTypeDB);

            // Initialiser le résultat à la routine appelante.
            $reussite = true;

            // Exécuter la commande.
            // Si aucune erreur n'est survenue
            if (false == $prep->execute()) {
                error_log("Erreur lors de la modification d'un objet de la classe {$classe}: ".$conn->errorInfo()[2]);
                $reussite = false;
            }

            // Fermer la connexion.
            $conn = null;

            // Retourner le résultat à la routine appelante.
            return $reussite;
        }
    }
}
