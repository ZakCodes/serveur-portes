<?php

namespace PPS {
    class TBaseDonnees {
        private static $sourceBD = 'mysql:host=localhost;dbname=serveur-portes';
        private static $nomUsager = 'root';
        private static $motDePasse = '';

        private static $conn;

        private function __construct() {
        }

        public static function getBaseDonnee() {
            if (!isset(self::$conn)) {
                //Connection à la base de données
                self::$conn = new \PDO(self::$sourceBD, self::$nomUsager, self::$motDePasse);
            }
            return self::$conn;
        }
    }
}
