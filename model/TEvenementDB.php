<?php

namespace PPS {
    class TEvenementDB extends TModelDB {
        /*!
            @brief  Voir TModelDB::props
        */
        public static function props(): array {
            return [
                'id' => ['id', \PDO::PARAM_INT],
                'idPorte' => ['id_porte', \PDO::PARAM_INT],
                'dateHeure' => ['date_heure', \PDO::PARAM_INT],
                'type' => ['type', \PDO::PARAM_INT],
                'idUsager' => ['id_usager', \PDO::PARAM_INT],
            ];
        }

        /*!
            @brief  Voir TModelDB::table
        */
        public static function table(): string {
            return 'evenement';
        }

        /*!
            @brief  Voir TModelDB::defaut
        */
        public static function defaut() {
            return new TEvenement(0, 0, '', 0, 0);
        }
    }
}
