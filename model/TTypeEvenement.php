<?php

namespace PPS {
    //! Enum représentant le type d'un évènement
    abstract class TTypeEvenement {
        const teVerrouillee = 0;
        const teDeverrouillee = 1;
        const teOuverte = 2;
        const teFermee = 3;
        const teDemarrage = 4;
    }
}
