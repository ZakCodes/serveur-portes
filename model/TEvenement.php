<?php

namespace PPS {
    class TEvenement {
        public $id;
        public $idPorte;
        public $dateHeure;
        public $type;
        public $idUsager;

        public function __construct($id, $idPorte, $dateHeure, $type, $idUsager) {
            $this->id = $id;
            $this->idPorte = $idPorte;
            $this->dateHeure = $dateHeure;
            $this->type = $type;
            $this->idUsager = $idUsager;
        }
    }
}
