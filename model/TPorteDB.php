<?php

namespace PPS {
    class TPorteDB extends TModelDB {
        /*!
            @brief  Voir TModelDB::props
        */
        public static function props(): array {
            return [
                'id' => ['id', \PDO::PARAM_INT],
                'nom' => ['nom', \PDO::PARAM_STR],
                'motDePasse' => ['mot_de_passe', \PDO::PARAM_STR],
                'url' => ['url', \PDO::PARAM_STR],
                'fermee' => ['fermee', \PDO::PARAM_BOOL],
                'verrouillee' => ['verrouillee', \PDO::PARAM_BOOL],
            ];
        }

        /*!
            @brief  Voir TModelDB::table
        */
        public static function table(): string {
            return 'porte';
        }

        /*!
            @brief  Voir TModelDB::defaut
        */
        public static function defaut() {
            return new TPorte(0, '', '', '', false, false);
        }

        /*!
            @brief  Trouve une porte dans la base de données avec le nom et le
                    mot de passe donné
            @retval null Une porte avec les caractèristiques données n'a pas pu
                         être trouvée
            @return TPorte La porte authentifiée
        */
        public static function authentifierPorte(
            //! Nom de la porte à authentifier
            string $nom,
            //! Mot de passe de la porte à authentifier
            string $mdp
        ): ?TPorte {
            // Initialiser le résultat à la valeur de porte inexistante.
            $porte = null;

            // Établir la connexion avec la base de données.
            $conn = TBaseDonnees::getBaseDonnee();

            // Obtenir les données sur la porte.
            $sql = "SELECT * FROM porte WHERE nom='{$nom}' AND mot_de_passe='{$mdp}';";

            //Placer les données de la porte dans une classe porte
            $resultat = $conn->query($sql);
            $ligne = $resultat->fetch();

            if ($ligne) {
                $porte = new TPorte(
                    $ligne['id'],
                    $ligne['nom'],
                    $ligne['mot_de_passe'],
                    $ligne['url'],
                    $ligne['fermee'],
                    $ligne['verrouillee']
                );
            }

            //Fermer la connexion
            $conn = null;

            return $porte;
        }
    }
}
