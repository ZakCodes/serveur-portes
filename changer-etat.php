<?php

require_once 'model/TBaseDonnees.php';
require_once 'model/TModelDB.php';
require_once 'model/TTypeEvenement.php';
require_once 'model/TEvenement.php';
require_once 'model/TEvenementDB.php';
require_once 'model/TPorte.php';
require_once 'model/TPorteDB.php';

use PPS\TEvenement;
use PPS\TEvenementDB;
use PPS\TPorteDB;
use PPS\TTypeEvenement;

// Codes de réponses HTTP
define('CODE_ERREUR_AUTHENTIFICATION', 401);
define('CODE_REQUETE_INVALIDE', 400);
define('CODE_REUSSITE', 204);

// Index de l'entête d'authorization dans $GLOBALS.
define('ENTETE_AUTHORIZATION', 'authorization');

// Schéma d'authentification attendu dans les requêtes.
define('SCHEME_AUTHENTIFICATION', 'basic');

/*!
    @brief  Ajoute un évènement à la base de données avec l'id de porte et le
            type donné et la date et l'heure actuelle
*/
function ajouterEvenement(
    //! ID de la porte associée à l'évènement
    int $idPorte,
    //! Type de l'évènement à ajouter
    int $type
) {
    // Ajouter un évènement à la base de données.
    TEvenementDB::inserer(
        new TEvenement(
            0,
            $idPorte,
            new DateTime(),
            $type,
            -1
        )
    );
}

// Initialiser le code de réponse HTTP au code d'erreur d'authentification.
$codeReponse = CODE_ERREUR_AUTHENTIFICATION;

// Initialiser la porte à null.
$porte = null;

// Obtenir le préfixe et sa taille pour l'entête d'authentification
$prefixEntete = SCHEME_AUTHENTIFICATION.' ';
$taillePrefix = strlen($prefixEntete);

// Obtenir la liste d'entêtes.
$entetes = array_change_key_case(apache_request_headers());

// Si l'entête comporte une entête d'authorization.
if (isset($entetes[ENTETE_AUTHORIZATION])) {
    $entete = $entetes[ENTETE_AUTHORIZATION];

    // Si le schema d'authorization est celui attendu
    $scheme = substr($entete, 0, $taillePrefix);
    if (strtolower($scheme) == $prefixEntete) {
        try {
            // Décoder les identifiants de la requête en base 64.
            $identifiants = array_map(
                function ($element) {
                    return base64_decode($element);
                },
                explode(':', substr($entete, $taillePrefix))
            );

            // Si le nom et le mot de passe ont pu être décodés
            if (isset($identifiants[0], $identifiants[1])) {
                // Trouver une porte avec le nom et le mot de passe donné
                $porte = TPorteDB::authentifierPorte($identifiants[0], $identifiants[1]);
            }
        } catch (Exception $e) {
        }
    }
}

if (null != $porte) {
    // Assigner le code de réussite au code de réponse.
    $codeReponse = CODE_REUSSITE;

    // Décoder le corps de la requête.
    $corps = file_get_contents('php://input');

    // Initialiser la liste de sémaphores à null.
    $semaphores = [];

    // Si le corps contient au moins trois caractères
    if (strlen($corps) >= 3) {
        // Convertir les trois premiers caractères du corps de la
        // requête en une liste de sémaphores.
        $semaphores = array_map(function ($c) {
            // Initialiser un résultat à true.
            $resultat = true;

            // Si le caractère est un 0.
            if ('0' == $c) {
                // Assigner false au résultat.
                $resultat = false;
            }
            // Sinon si le caractère est invalide
            elseif ('1' != $c) {
                // Assigner le code de requête invalide au code de réponse.
                $codeReponse = CODE_REQUETE_INVALIDE;
            }

            // Retourner le résultat à la routine appelante.
            return $resultat;
        }, str_split(substr($corps, 0, 3)));
    } else {
        // Assigner le code de requête invalide au code de réponse.
        $codeReponse = CODE_REQUETE_INVALIDE;
    }

    // Si la requête est valide
    if (CODE_REUSSITE == $codeReponse) {
        // Si l'état du verrou a changé
        if ($semaphores[0] != $porte->verrouillee) {
            // Modifier l'état du verrou.
            $porte->verrouillee = $semaphores[0];

            // Ajouter un évènement de changement d'état du verrou à la base de données.
            ajouterEvenement($porte->id, $porte->verrouillee ? TTypeEvenement::teVerrouillee : TTypeEvenement::teDeverrouillee);
        }

        // Si l'état d'ouverture de la porte a changée
        if ($semaphores[1] != $porte->fermee) {
            // Modifier la fermeture de la porte.
            $porte->fermee = $semaphores[1];

            // Ajouter un évènement de modification de la fermeture de la porte.
            ajouterEvenement($porte->id, $porte->fermee ? TTypeEvenement::teFermee : TTypeEvenement::teOuverte);
        }

        // Si le contrôleur vient de démarrer
        if ($semaphores[2]) {
            // Ajouter un évènement de démarrage du contrôleur de porte.
            ajouterEvenement($porte->id, TTypeEvenement::teDemarrage);
        }

        // Modifier la porte dans la base de données.
        TPorteDB::modifier($porte);
    }
}

http_response_code($codeReponse);
