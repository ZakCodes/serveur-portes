# Serveur de contrôleurs de portes
Ce logiciel est un serveur écrit en PHP pour permettre à des utilisateurs de modifier l'état de portes.

## Installation

### [Installer XAMPP]
La manière la plus simple d'utiliser ce projet est d'installer [XAMPP](https://www.apachefriends.org/) que vous pouvez installer après l'avoir téléchargé [ici][Installer XAMPP].

Ce logiciel inclut les  deux seules choses nécessaires à ce projet: un serveur PHP et une base de données. En plus de cela, il est disponible pour Linux, MacOS et Windows.

### Copier le projet dans vos projets locaux
Vous devrez placer le dossier de ce projet dans votre dossier `/opt/lampp/htdocs` sur Mac et Linux ou votre dossier `C:\xampp\htdocs` sur Windows.

Si vous avez accès à ce projet sur GDrive, vous pouvez le télécharger à partir de là.  
Sinon, vous pouvez le télécharger de GitLab à [ce lien](https://gitlab.com/ZakCodes/serveur-portes).

### Démarrer le serveur
Une fois que vous aurez installé [XAMPP], vous pouvez ouvrir son interface graphique et démarrer tous les services.
![Démarrer le serveur](images/demarrer-services.png)

Le serveur est désormais accessible à l'adresse <http://localhost> dans votre naviguateur.

### Configurer la base de données
Afin de configurer la base de données, rendez vous à l'adresse <http://localhost/phpmyadmin/server_import.php>.  

Une fois là, appuyez sur le bouton `New` dans la barre à gauche de l'écran pour créer une nouvelle base de données.  

Une nouvelle page apparaîtra vous permettant de configurer la base de données. Configurer son nom pour qu'il soit `serveur-portes`, mais ne changez aucune autre option. Appuyez ensuite sur `Go`.

Maintenant votre base de données créée, nous allons y importer des tables et des données. Pour se faire, cliquez sur le bouton `Import` dans la barre d'outils.  
Lorsque la nouvelle page aura fini de charger, appuyez sur le bouton Browse et sélectionnez le fichier `schema.sql`.
Ne changez aucune autre option et appuyez sur le bouton `Go` tout en bas de la page.

### Accéder au site
Vous devriez maintenant pouvoir accéder au serveur à l'adresse <https://localhost/serveur-portes/>.

### Pages du site
Le site contient 4 pages:

###### Connexion (`/serveur-portes/index.php?action=login`)
Ceci est la page de connexion à un compte. L'utilisateur doit entrer son numéro d'utilisateur, qui est son numéro d'étudiant ou d'employé, et son mot de passe pour se connecter.  
Lorsqu'un utilisateur n'est pas connecté et qu'il accède à la page `serveur/index.php` avec n'importe quel paramètre, il sera automatiquement redirigé vers cette page.

###### Déconnexion (`/serveur-portes/index.php?action=logout`)
Lorsqu'un utilisateur accède à cette page en appuyant sur le bouton de déconnexion, il est déconnecté de son compte et redirigé vers la page de connexion.

###### Affichage des portes (`/serveur-portes/index.php?action=portes`)
Cette page permet de voir l'état de toutes les portes et de les verrouillées et de les déverrouiller.  
En plus du URL donné, un utilisateur connecté pourra accéder à cette page directement à l'adresse `/serveur-portes/index.php`.  

###### Notification de changement d'état des contrôleurs de portes (`/serveur-portes/changer-etat.php`)
**Cette page est uniquement destinée à être utilisée par les contrôleurs de portes. Si vous tentez d'accéder à cette page à partir de votre naviguateur, vous obtiendrez un code d'accès refusé (401).**  
Elle permet aux contrôleurs de portes de notifier le serveur de leur état. Pour s'authentifier, les contrôleurs de portes doivent utiliser le schema d'authentification [`Basic`](https://en.wikipedia.org/wiki/Basic_access_authentication). Il consiste à envoyer une entête HTTP suivant le format `Authorization: Basic <nom>:<mot de passe>`. Le `nom` et le `mot de passe` doivent être les mêmes qui se trouvent dans la base de données du serveur et doivent être encodés en [Base64](https://fr.wikipedia.org/wiki/Base64).

### Dépendances
* [FontAwesome 5](https://fontawesome.com/)
* [Bootstrap 4.4](https://getbootstrap.com/docs/4.4/)

## À faire
### Inclure une image du schéma de la base de données à la documentation technique
Il serait utile d'avoir une image du schéma de la base de données dans la documentation pour aider les gens à comprendre plus facilement ce qu'elle contient et les relations entre ses données.

### [Annotations de types pour les membres données des classes](https://laravel-news.com/php7-typed-properties)
À partir de PHP 7.4, l'annotation des membres données des classes pourra ce faire tout comme en C/C++.  
Voici un exemple de comment elle doit être faite en ce moment:
```php
class TClasse {
    /** @var int $toto */
    public $toto;
}
```

Ceci pourra bientôt être remplacé par:
```php
class TClasse {
    public int $toto;
}
```

Lorsque cette fonctionnalité sera disponible, il suffira de chercher toute les instances de `/** @var` dans le code et de les remplacer.

<!-- Liens -->
[XAMPP]: https://www.apachefriends.org/index.html
[Installer XAMPP]: https://www.apachefriends.org/download.html
